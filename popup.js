document.addEventListener('DOMContentLoaded', function() {

  var visibility = false;
  var element = document.getElementsByClassName("visibility-btn");
  for(var i = 0; i < element.length; i++)
  {
      element[i].addEventListener("click", changeVisibility);
  }

  var element = document.getElementsByClassName("copy-btn");
  for(var i = 0; i < element.length; i++)
  {
      element[i].addEventListener("click", copy);
  }

  var deriveKey = document.getElementById('derive-key');
  deriveKey.addEventListener('click', deriveAKey, false);

  function changeVisibility()
  {
    var aes_key_fields = document.getElementsByClassName("aes-key");
    var visibility_fields = document.getElementsByClassName("visibility-btn-icon");
    var type = "text";
    var add = "fa-eye-slash";
    var remove = "fa-eye";

    if (visibility)
    {
      type = "password";
      add = "fa-eye";
      remove = "fa-eye-slash";
    }

    for(var i = 0; i < aes_key_fields.length; i++)
    {
        aes_key_fields[i].type = type;
    }

    for(var i = 0; i < visibility_fields.length; i++)
    {
        visibility_fields[i].classList.remove(remove);
        visibility_fields[i].classList.add(add);
    }

    visibility = !visibility;
  }

  function copy(event)
  {
    var copyTextarea = document.querySelector('#aes-key-hidden');
    copyTextarea.select();
    document.execCommand('copy');

    var element = document.getElementsByClassName("aes-key");
    var previous_value = document.getElementById("aes-key-hidden").value;
    for(var i = 0; i < element.length; i++)
    {
        element[i].type = 'text';
        element[i].value = 'copied';
        element[i].classList.add('copied');
    }

    setTimeout(function()
    {
      var element = document.getElementsByClassName("aes-key");
      for(var i = 0; i < element.length; i++)
      {
        if (visibility)
        {
          element[i].type = 'text';
        }
        else
        {
          element[i].type = 'password';
        }

          element[i].value = previous_value;
          element[i].classList.remove('copied');
      }
    }, 1000);
  }

  function deriveAKey()
  {
      document.getElementById("aes-key-hidden").value = '';
      var element = document.getElementsByClassName("aes-key");
      for(var i = 0; i < element.length; i++)
      {
          element[i].classList.add('aes-key-animation');
          element[i].value = '';
      }

      var user = document.getElementById("user").value;
      var url = document.getElementById("url").value;
      var iterations = 1000000;
      var hash = "SHA-512";
      var password = document.getElementById("password").value;

      sha512(user).then(function (user_hash) {
          sha512(url).then(function (url_hash) {
              sha512(user_hash + url_hash).then(function (salt) {

      window.crypto.subtle.importKey(
          "raw",
          stringToArrayBuffer(password),
          {"name": "PBKDF2"},
          false,
          ["deriveKey"]).
      then(function(baseKey){
          return window.crypto.subtle.deriveKey(
              {
                  "name": "PBKDF2",
                  "salt": stringToArrayBuffer(salt),
                  "iterations": iterations,
                  "hash": hash
              },
              baseKey,
              {"name": "AES-CBC", "length": 128}, // Key we want
              true,                               // Extrable
              ["encrypt", "decrypt"]              // For new key
              );
      }).
      // Export it so we can display it
      then(function(aesKey) {
          return window.crypto.subtle.exportKey("raw", aesKey);
      }).
      // Display it in hex format
      then(function(keyBytes) {
              var hexKey = encode_ascii85(keyBytes);
              document.getElementById("aes-key-hidden").value = hexKey;
              var element = document.getElementsByClassName("aes-key");
              for(var i = 0; i < element.length; i++)
              {
                  element[i].classList.remove('aes-key-animation');
                  element[i].value = hexKey;
              }
      }).
      catch(function(err) {
          alert("Key derivation failed: " + err.message);
      });
  })})})}


  // Utility functions
  function stringToArrayBuffer(string)
  {
      var encoder = new TextEncoder("utf-8");
      return encoder.encode(string);
  }

  function arrayBufferToHexString(arrayBuffer)
  {
      var byteArray = new Uint8Array(arrayBuffer);
      var hexString = "";
      var nextHexByte;

      for (var i=0; i<byteArray.byteLength; i++)
      {
          nextHexByte = byteArray[i].toString(16);  // Integer to base 16
          if (nextHexByte.length < 2)
          {
              nextHexByte = "0" + nextHexByte;     // Otherwise 10 becomes just a instead of 0a
          }
          hexString += nextHexByte;
      }
      return hexString;
  }

  function arrayBufferToBase64(buffer)
  {
      var binary = '';
      var bytes = new Uint8Array(buffer);
      var len = bytes.byteLength;
      for (var i = 0; i < len; i++)
      {
          binary += String.fromCharCode( bytes[ i ] );
      }
      return window.btoa( binary );
  }

  function encode_ascii85(buffer)
  {
      var arr = new Uint32Array( buffer );

      var _chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ.-:+=^!/*?&<>()[]{}@%$#";

      if (!arr)
      {
          return null;
      }

      var out = "", c = _chars;

      for (var i = 0; i < arr.length; ++i)
      {
          var word = arr[i];
          var value = 0;
          for (var j = 0; j < 4; ++j)
          {
              var byteChunk = (word >>> 8*(4 - j - 1)) & 0xFF;
              value = value*256 + byteChunk;
          }
          var divisor = 85*85*85*85;
          while (divisor)
          {
              out += c.charAt(Math.floor(value/divisor) % 85);
              divisor = Math.floor(divisor/85);
          }
      }

      var encodedSize = arr.length*5;
      if (out.length !== encodedSize)
      {
          console.error("Bad Z85 conversion!");
      }

      return out;
  }

  function hex(buffer)
  {
      var hexCodes = [];
      var view = new DataView(buffer);
      for (var i = 0; i < view.byteLength; i += 4)
      {
          // Using getUint32 reduces the number of iterations needed (we process 4 bytes each time)
          var value = view.getUint32(i)
          // toString(16) will give the hex representation of the number without padding
          var stringValue = value.toString(16)
          // We use concatenation and slice for padding
          var padding = '00000000'
          var paddedValue = (padding + stringValue).slice(-padding.length)
          hexCodes.push(paddedValue);
      }

      // Join all the hex strings into one
      return hexCodes.join("");
  }

  function sha512(str)
  {
      var buffer = new TextEncoder("utf-8").encode(str);
      return crypto.subtle.digest("SHA-512", buffer).then(function(hash)
      {
          return hex(hash);
      });
  }

}, false);
